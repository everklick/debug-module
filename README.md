# Debug Module

Version 2.4.7

A simple and lightweight debug module that can be used on any PHP project.

## Installation and usage

All you need is the `debug.php` file from this repository. Simply include it in your main `index.php` file and you are ready to use the `debug()` function.

Example:

    <?php
	include_once 'debug.php';

	debug( 'This is an example', time() );

## Documentation

You can find more examples, configuration options and documentation on the project website: https://philippstracker.com/debug-module/
